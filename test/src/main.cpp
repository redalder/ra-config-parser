#include <RedAdler/MemoryManagement.h>
#include <iostream>
#include <RedAdler/ConfigParser.h>

int main() {
    TestClass testClass = TestClass();

    auto i = Accessor<int, false, true, true>(testClass.pool);

    i.Get() = 45879;

    std::cout << i.Get() << std::endl;
}